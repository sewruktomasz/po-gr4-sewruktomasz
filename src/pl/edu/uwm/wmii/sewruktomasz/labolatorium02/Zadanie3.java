package pl.edu.uwm.wmii.sewruktomasz.labolatorium02;

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class Zadanie3 {
    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for(int j = 0; j<n; j++){
            tab[j]  = r.nextInt((maxWartosc - minWartosc) + 1) + minWartosc;
        }
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj m, n i k");
        int m = in.nextInt(), n = in.nextInt(), k = in.nextInt();
        int[][] a, b, c;
        a = new int[m][n];
        b = new int[n][k];
        c = new int[m][k];

        for (int i = 0; i < m; i++)
            generuj(a[i], n, -10, 10);
        for (int i = 0; i < n; i++)
            generuj(b[i], k, -10, 10);

        System.out.println("a = ");
        for (int[] row : a)
            System.out.println(Arrays.toString(row));
        System.out.println("------------------------------------------------------------");

        System.out.println("b = ");
        for (int[] row : b)
            System.out.println(Arrays.toString(row));

        for(int i = 0; i < m; i++)
            for (int j = 0; j < k; j++) {
                c[i][j] = 0;
                for (int l = 0; l < n; l++) {
                    c[i][j] += a[i][l] * b[l][j];
                }
            }

         System.out.println("-------------------------------------------------------------");

        System.out.println("c = ");
        for (int[] row: c)
            System.out.println(Arrays.toString(row));
    }
}
