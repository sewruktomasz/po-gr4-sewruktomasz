package pl.edu.uwm.wmii.sewruktomasz.labolatorium02;
import java.util.Scanner;
import java.util.Random;

public class Zadanie1d {
    public static void main(String[] arg){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt(), dodatnieSuma=0, ujemneSuma=0;
        int[] tab = new int[n];

        Random rand = new Random();



        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;
            if(tab[i] > 0) dodatnieSuma+=tab[i];
            if(tab[i] < 0) ujemneSuma+=tab[i];
        }

        System.out.println("dodatnie: "+dodatnieSuma);
        System.out.println("ujemne: "+ujemneSuma);

    }
}
