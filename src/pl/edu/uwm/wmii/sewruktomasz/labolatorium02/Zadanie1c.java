package pl.edu.uwm.wmii.sewruktomasz.labolatorium02;
import java.util.Scanner;
import java.util.Random;

public class Zadanie1c {
    public static void main(String[] arg){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt(), najwieksza=-1000, ileRazyNajwieksza = 0;
        int[] tab = new int[n];

        Random rand = new Random();



        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;
            if(tab[i]>najwieksza)
                najwieksza = tab[i];
        }
        for(int i = 0; i < n; i++)
            if(tab[i]==najwieksza)
                ileRazyNajwieksza++;

        System.out.println("Liczba: "+najwieksza+" wystepuje razy:"+ileRazyNajwieksza);



    }
}
