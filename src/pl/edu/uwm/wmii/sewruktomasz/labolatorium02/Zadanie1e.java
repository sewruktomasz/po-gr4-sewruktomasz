package pl.edu.uwm.wmii.sewruktomasz.labolatorium02;
import java.util.Scanner;
import java.util.Random;

public class Zadanie1e {
    public static void main(String[] arg){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt(), iloscElementowCiaguDodatniego = 0, najdluzszyCiag = 0;
        int[] tab = new int[n];

        Random rand = new Random();


        tab[0] = rand.nextInt(1999) - 999;
        for(int i = 1; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;

            if(tab[i-1]>0 && tab[i]>0) {
                iloscElementowCiaguDodatniego++;
                if(iloscElementowCiaguDodatniego>najdluzszyCiag) najdluzszyCiag = iloscElementowCiaguDodatniego;
            }
            else
                iloscElementowCiaguDodatniego = 0;
        }

        System.out.println(najdluzszyCiag);


    }
}
