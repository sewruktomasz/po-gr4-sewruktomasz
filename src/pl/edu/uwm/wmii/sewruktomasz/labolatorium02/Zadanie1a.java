package pl.edu.uwm.wmii.sewruktomasz.labolatorium02;
import java.util.Scanner;
import java.util.Random;

public class Zadanie1a {
    public static void main(String[] arg){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt(), parzyste = 0, nieparzyste = 0, liczba;
        int[] tab = new int[n];

        Random rand = new Random();



        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;
            if(tab[i]%2==0) parzyste++;
            else nieparzyste++;
        }

        System.out.println("parzyste: "+parzyste);
        System.out.println("nieparzyste: "+nieparzyste);

    }
}
