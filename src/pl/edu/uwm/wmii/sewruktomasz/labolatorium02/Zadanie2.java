package pl.edu.uwm.wmii.sewruktomasz.labolatorium02;
import java.util.Scanner;
import java.util.Random;

public class Zadanie2 {


    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random r = new Random();
        for(int j = 0; j<n; j++){
            tab[j]  = r.nextInt((maxWartosc - minWartosc) + 1) + minWartosc;
        }
    }

    //zadanie a
    public static int ileNieparzystych(int tab[]){
        int ile = 0;
        for(int i = 0;i<tab.length;i++)
            if(tab[i]%2!=0)
                ile++;
        return ile;
    }

    public static int ileParzystych(int tab[]){
        int ile = 0;
        for(int i = 0;i<tab.length;i++)
            if(tab[i]%2==0)
                ile++;
        return ile;
    }
    //---------------------------------------------


    //zadanie b
    public static int ileDodatnich(int tab[]){
        int ile=0;
        for (int i: tab)
            if(i>0) ile++;
        return ile;
    }

    public static int ileUjemnych(int tab[]){
        int ile=0;
        for (int i: tab)
            if(i<0) ile++;
        return ile;
    }

    public static int ileZerowych(int tab[]){
        int ile=0;
        for (int i: tab)
            if(i==0) ile++;
        return ile;
    }
    //---------------------------------------------


    //zadanie c
    public static int ileMaksymalnych(int tab[]){
        int najwieksza = -1000, ile=0;
        for(int i = 0; i < tab.length; i++) {
            if(tab[i]>najwieksza)
                najwieksza = tab[i];
        }
        for(int i = 0; i < tab.length; i++)
            if(tab[i]==najwieksza)
                ile++;
        return ile;
    }
    //---------------------------------------------


    //zadanie d
    public static int sumaDodatnich(int tab[]){
        int suma = 0;
        for(int i: tab)
            if(i>0) suma+=i;
        return suma;
    }

    public static int sumaUjemnych(int tab[]) {
        int suma = 0;
        for (int i : tab)
            if(i < 0) suma += i;
        return suma;
    }
    //---------------------------------------------


    //zadanie e
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]){
        int iloscElementowCiaguDodatniego = 1, najdluzszyCiag = 0;

        for(int i = 1; i < tab.length; i++) {

            if(tab[i-1]>0 && tab[i]>0) {
                iloscElementowCiaguDodatniego++;
                if(iloscElementowCiaguDodatniego>najdluzszyCiag) najdluzszyCiag = iloscElementowCiaguDodatniego;
            }
            else
                iloscElementowCiaguDodatniego = 0;
        }

        return najdluzszyCiag;
    }
    //---------------------------------------------


    //zadanie f
    public static void signum(int tab[]){
        for(int i = 0; i<tab.length;i++){
            if(tab[i]<0) tab[i] = -1;
            else if(tab[i]>0) tab[i] = 1;
            System.out.print(tab[i]+", ");
        }
    }
    //---------------------------------------------


    //zadanie g
    public static void odwrocFragment(int tab[], int lewy, int prawy){
        for(int i = prawy-1; i>=lewy-1;i--)
            System.out.print(tab[i]+", ");
    }
    //---------------------------------------------

    public static void wyswietlTablice(int tab[]){
        System.out.print("Wyrazy tablicy to: ");
        for (int i: tab)
            System.out.print(i+"; ");
        System.out.println();
    }




    public static void main(String[] arg){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab = new int[n];

        Random rand = new Random();

        generuj(tab, n, -999, 999);
        int[] tabKopia = tab;
        wyswietlTablice(tab);



        System.out.println("ileNieparzystych: "+ ileNieparzystych(tab));
        System.out.println("ileParzystych: "+ ileParzystych(tab));
        System.out.println("ileDodatniich: "+ ileDodatnich(tab));
        System.out.println("ileUjemnych: "+ ileUjemnych(tab));
        System.out.println("ileZerowych: "+ ileZerowych(tab));
        System.out.println("ileMaksymalnych: "+ ileMaksymalnych(tab));
        System.out.println("sumaDodatnich: "+ sumaDodatnich(tab));
        System.out.println("sumaUjemnych: "+ sumaUjemnych(tab));
        System.out.println("dlugoscMaksymalnegoCiaguDodatnich: "+ dlugoscMaksymalnegoCiaguDodatnich(tab));

        System.out.print("odwrocFragment: "); odwrocFragment(tab, 2, 5); System.out.println("");
        System.out.print("signum: "); signum(tabKopia); System.out.println("");


    }

}
