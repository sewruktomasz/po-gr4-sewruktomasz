package pl.edu.uwm.wmii.sewruktomasz.labolatorium02;
import java.util.Scanner;
import java.util.Random;

public class Zadanie1b {
    public static void main(String[] arg){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt(), dodatnie=0, ujemne=0, zera=0;
        int[] tab = new int[n];

        Random rand = new Random();



        for(int i = 0; i < n; i++) {
            tab[i] = rand.nextInt(1999) - 999;
            if(tab[i] > 0) dodatnie++;
            if(tab[i] < 0) ujemne++;
            if(tab[i] == 0) zera++;
        }

        System.out.println("dodatnie: "+dodatnie);
        System.out.println("ujemne: "+ujemne);
        System.out.println("zera: "+zera);

    }
}
