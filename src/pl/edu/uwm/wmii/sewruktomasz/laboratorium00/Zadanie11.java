package pl.edu.uwm.wmii.sewruktomasz.laboratorium00;

public class Zadanie11 {

    public static void main(String[] args) {
        System.out.println("Stoi na stacji lokomotywa, \n" +
                "Ciężka, ogromna i pot z niej spływa: \n" +
                "Tłusta oliwa. \n" +
                "Stoi i sapie, dyszy i dmucha, \n" +
                "Żar z rozgrzanego jej brzucha bucha: \n" +
                "Buch - jak gorąco! \n" +
                "Uch - jak gorąco! \n" +
                "Puff - jak gorąco! \n" +
                "Uff - jak gorąco! \n" +
                "Już ledwo sapie, już ledwo zipie, \n" +
                "A jeszcze palacz węgiel w nią sypie.");
    }
}

