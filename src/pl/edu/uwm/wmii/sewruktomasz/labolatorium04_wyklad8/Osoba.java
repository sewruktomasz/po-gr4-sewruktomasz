package pl.edu.uwm.wmii.sewruktomasz.labolatorium04_wyklad8;

import java.time.LocalDate;
import java.time.LocalDateTime;

abstract class Osoba {
    public Osoba(String nazwisko, String imie, LocalDate dataUrodzenia, boolean plec) {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko() {
        return nazwisko;
    }

    private String nazwisko;
    private String imie;
    private LocalDate dataUrodzenia;
    private boolean plec;
}
