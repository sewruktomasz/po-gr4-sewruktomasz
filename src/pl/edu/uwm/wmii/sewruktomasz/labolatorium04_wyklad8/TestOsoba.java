package pl.edu.uwm.wmii.sewruktomasz.labolatorium04_wyklad8;

import java.time.LocalDate;
import java.util.*;


public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalski","Jan", LocalDate.of(1965,01,03), false, 50000, LocalDate.of(2000,12,17));
        ludzie[1] = new Student("Nowak","Malgorzata", LocalDate.of(1998,04,30), true, "informatyka", 4.35);
        //ludzie[2] = new Osoba("Glowacki", "Michal", "1997-02-09", 0);

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}









