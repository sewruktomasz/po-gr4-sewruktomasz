package pl.edu.uwm.wmii.sewruktomasz.labolatorium04_wyklad8;

import java.time.LocalDate;

class Student extends Osoba {
    public Student(String nazwisko, String imie, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen) {
        super(nazwisko, imie, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis() {
        return "kierunek studiów: " + kierunek;
    }

    private String kierunek;
    private double sredniaOcen;
}