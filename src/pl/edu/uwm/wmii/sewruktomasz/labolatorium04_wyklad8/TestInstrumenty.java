package pl.edu.uwm.wmii.sewruktomasz.labolatorium04_wyklad8;

import pl.imiajd.sewruk.Instrument;

import java.time.LocalDate;
import java.util.ArrayList;

import pl.imiajd.sewruk.*;

public class TestInstrumenty {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Flet("Fletpol", LocalDate.of(2012, 2, 15)));
        orkiestra.add(new Fortepian("Fortopol", LocalDate.of(2004, 6, 11)));
        orkiestra.add(new Skrzypce("Skrzypkol", LocalDate.of(2014, 3, 20)));
        orkiestra.add(new Flet("Music World", LocalDate.of(2018, 2, 10)));
        orkiestra.add(new Skrzypce("Skrzypkol", LocalDate.of(2015, 1, 12)));
        for (Instrument i : orkiestra)
            System.out.println(i.dzwiek());
        System.out.println(orkiestra.toString());
    }
}
