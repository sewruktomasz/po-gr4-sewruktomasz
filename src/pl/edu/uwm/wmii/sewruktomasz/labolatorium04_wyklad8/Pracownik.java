package pl.edu.uwm.wmii.sewruktomasz.labolatorium04_wyklad8;

import java.time.LocalDate;
import java.time.LocalDateTime;

class Pracownik extends Osoba {
    public Pracownik(String nazwisko, String imie, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, imie, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }

    public String getOpis() {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    public java.time.LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }

    private double pobory;
    private java.time.LocalDate dataZatrudnienia;
}
