package pl.edu.uwm.wmii.sewruktomasz.labolatorium03_wyklad4;

import java.util.Arrays;
import java.lang.String;

public class Zadanie1 {
    public static int countChar(String str, char c){
        int counter = 0;
        for(int i = 0; i < str.length(); i++)
            if(str.charAt(i) == c) {
                counter++;
            }
        return counter;
    }

    public static int countSubStr(String str, String subStr){
        int counter = 0;
        for(int i = 0; i < str.length(); i++){
            if(str.indexOf(subStr) != -1){
                counter++;
                str = str.replaceFirst(subStr,"");
            }
        }

        return counter;
    }

    public static String middle(String str){
        if(str.length()%2==0) return Character.toString(str.charAt(str.length()/2-1))+Character.toString(str.charAt(str.length()/2));
        else return Character.toString(str.charAt(str.length()/2));
    }

    public static String repeat(String str, int n){
        String konkatenacja = "";
        for(int i = 0; i<n;i++){
            konkatenacja += str;
        }
        return konkatenacja;
    }

    public static int[] where(String str, String subStr) {
        int[] result = new int[countSubStr(str, subStr)];
        for (int i = 0, j = 0; i >= 0;) {
            i = str.indexOf(subStr, i + 1);
            if (i != -1)
                result[j++] = i;
        }
        return result;
    }

    public static String change(String str) {
        StringBuffer result = new StringBuffer();
        for (char c : str.toCharArray())
            result.append((int)c >= 65 && (int)c <= 90 ? (char)((int)c + 32) : (char)((int)c - 32));
        return result.toString();
    }

    public static String nice(String str) {
        StringBuffer reversed = new StringBuffer(str);
        reversed.reverse();
        StringBuffer result = new StringBuffer();
        for (int i = 1, j = 0; j < reversed.length(); i++) {
            if (i % 4 == 0)
                result.append('\'');
            else {
                result.append(reversed.charAt(j));
                j++;
            }
        }
        result.reverse();
        return result.toString();
    }
    public static String nice(String str, char sep, int pos) {
        StringBuffer reversed = new StringBuffer(str);
        reversed.reverse();
        StringBuffer result = new StringBuffer();
        for (int i = 1, j = 0; j < reversed.length(); i++) {
            if (i % (pos + 1) == 0)
                result.append(sep);
            else {
                result.append(reversed.charAt(j));
                j++;
            }
        }
        result.reverse();
        return result.toString();
    }

    public static void main(String[] arg){
       System.out.print("Zadanie 1 a): ");
       System.out.println(countChar("Domminik", 'm'));

       System.out.print("Zadanie 1 b): ");
       System.out.println(countSubStr("llalalatomlalato", "lal"));

       System.out.print("Zadanie 1 c): ");
       System.out.println(middle("Domik"));
       System.out.println(middle("Dominik"));

       System.out.print("Zadanie 1 d): ");
       System.out.println(repeat("hoho", 3));


       System.out.println("Zadanie 1 e): ");
       int[] tab;
       tab = where("tomtosgotoqqtoto", "to");
       for (int i = 0; i<tab.length;i++){
           System.out.println(tab[i]);
       }


       String napis = "DomiNIK";
       System.out.print("Zadanie 1 f): PRZED: "+napis+"; po modyfikacji: ");
       System.out.println(change(napis));


       System.out.print("Zadanie 1 g): ");
       System.out.println(nice(napis));


        System.out.print("Zadanie 1 h): ");
        System.out.println(nice(napis, '!', 2));
    }
}
