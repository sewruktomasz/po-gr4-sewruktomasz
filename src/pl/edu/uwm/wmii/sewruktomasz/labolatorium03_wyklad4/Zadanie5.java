package pl.edu.uwm.wmii.sewruktomasz.labolatorium03_wyklad4;

import java.math.BigDecimal;

public class Zadanie5 {
    public static BigDecimal kapital(BigDecimal k, int p, int n) {
        BigDecimal result = k;
        for (int i = 0; i < n; i++)
            result = result.multiply(BigDecimal.ONE.add(BigDecimal.valueOf(p).divide(BigDecimal.valueOf(100))));
        return result;
    }
    public static void main(String[] args) {
        System.out.println(kapital(BigDecimal.valueOf(1000), 5, 2));
    }
}