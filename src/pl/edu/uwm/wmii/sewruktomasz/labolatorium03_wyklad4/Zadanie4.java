package pl.edu.uwm.wmii.sewruktomasz.labolatorium03_wyklad4;

        import java.math.BigInteger;

public class Zadanie4 {
    public static BigInteger szachownica(int n) {
        BigInteger result = BigInteger.ZERO;
        for (int i = 0; i < n; i++) {
            result = result.add(BigInteger.valueOf(2).pow(i));
        }
        return result;
    }
    public static void main(String[] args) {
        int n = 8;

        System.out.println(szachownica(n));
    }
}