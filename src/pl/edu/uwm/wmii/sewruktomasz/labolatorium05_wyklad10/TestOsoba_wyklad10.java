package pl.edu.uwm.wmii.sewruktomasz.labolatorium05_wyklad10;

import pl.imiajd.sewruk.Osoba_wyklad10.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

public class TestOsoba_wyklad10{
    public static void main(String[] args){


        ArrayList<Osoba_wyklad10> grupa = new ArrayList<>();

        grupa.add(new Osoba_wyklad10("Lolson", LocalDate.of(1997,9,22)));
        grupa.add(new Osoba_wyklad10("Sewruk", LocalDate.of(1997,9,22)));
        grupa.add(new Osoba_wyklad10("Sewruk", LocalDate.of(1988,12,30)));
        grupa.add(new Osoba_wyklad10("Kowalky", LocalDate.of(1997,9,22)));
        grupa.add(new Osoba_wyklad10("Sewruk", LocalDate.of(1988,12,30)));



        //grupa.sort();
        
        System.out.println(grupa.toString());
    }
}
