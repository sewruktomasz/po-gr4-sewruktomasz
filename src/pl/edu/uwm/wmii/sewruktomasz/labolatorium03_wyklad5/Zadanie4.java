package pl.edu.uwm.wmii.sewruktomasz.labolatorium03_wyklad5;


import java.util.ArrayList;

public class Zadanie4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer>a){
        ArrayList<Integer>wynik = new ArrayList<Integer>();
        for(int i=a.size()-1;i>=0;i--){
            wynik.add(a.get(i));
        }
        return wynik;
    }


    public static void main(String[] arg){
        ArrayList<Integer>a = new ArrayList<Integer>();

        a.add(13);
        a.add(1);
        a.add(99);
        a.add(564);


        System.out.println(reversed(a));
    }
}

