package pl.edu.uwm.wmii.sewruktomasz.labolatorium03_wyklad5;

import java.util.ArrayList;

public class Zadanie1 {
    public static ArrayList<Integer> append(ArrayList<Integer>a, ArrayList<Integer>b){
        ArrayList<Integer>wynik = new ArrayList<Integer>();
        for(int i=0;i<a.size();i++)
            wynik.add(a.get(i));
        for(int i=0;i<b.size();i++)
            wynik.add(b.get(i));
        return wynik;
    }


    public static void main(String[] arg){
        ArrayList<Integer>a = new ArrayList<Integer>();
        ArrayList<Integer>b = new ArrayList<Integer>();

        a.add(13);
        a.add(1);
        a.add(99);
        a.add(564);
        b.add(3);
        b.add(1);
        b.add(3);
        b.add(98);
        b.add(98);
        b.add(14);
        b.add(7455);

        System.out.println(append(a,b));
    }
}
