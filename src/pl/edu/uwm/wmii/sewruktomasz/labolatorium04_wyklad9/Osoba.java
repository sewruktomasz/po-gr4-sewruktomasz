package pl.edu.uwm.wmii.sewruktomasz.labolatorium04_wyklad9;

import java.time.LocalDate;
import java.lang.Comparable;

public class Osoba implements Comparable<Osoba> {
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    @Override
    public String toString() {
        return this.getClass() + "[" + this.nazwisko + " " + this.dataUrodzenia + "]";
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }

        if (otherObject == null) {
            return false;
        }

        if (getClass() != otherObject.getClass()) {
            return false;
        }

        Osoba other = (Osoba) otherObject;

        return nazwisko.equals(other.nazwisko) && dataUrodzenia.equals(other.dataUrodzenia);
    }

    @Override
    public int compareTo(Object otherObject) {
        Osoba other = (Osoba) otherObject;
        int nazwiskoCompare = this.nazwisko.compareTo(other.nazwisko);
        int dataUrodzeniaCompare = this.dataUrodzenia.compareTo(other.dataUrodzenia);
        return nazwiskoCompare != 0 ? nazwiskoCompare : dataUrodzeniaCompare;
    }

    public static void main(String[] args) {
        System.out.println("fsdw");

    }
}


