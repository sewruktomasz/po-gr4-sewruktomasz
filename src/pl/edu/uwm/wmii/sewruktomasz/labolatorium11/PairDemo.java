package pl.edu.uwm.wmii.sewruktomasz.labolatorium11;

public class PairDemo {
    public static void main(String[] args) {
        Pair<String> p = new Pair<>("Tomasz", "Sewruk");
        p.swap();
        System.out.println(String.format("%s %s", p.getFirst(), p.getSecond()));
    }
}
