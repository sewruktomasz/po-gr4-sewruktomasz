package pl.edu.uwm.wmii.sewruktomasz.labolatorium11;

import java.time.LocalDate;
import java.util.ArrayList;

public class ArrayUtilDemo {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(3);
        a.add(5);
        a.add(9);
        a.add(10);
        a.add(11);
        ArrayList<Integer> b = new ArrayList<>();
        b.add(1);
        b.add(3);
        b.add(2);
        b.add(8);
        b.add(4);
        System.out.println(String.format("%s\n%s", ArrayUtil.isSorted(a) ? "true" : "false", ArrayUtil.isSorted(b) ? "true" : "false"));
        System.out.println(ArrayUtil.binSearch(a, 10));
        ArrayUtil.selectionSort(b);
        System.out.println(b);
        ArrayList<LocalDate> c = new ArrayList<>();
        c.add(LocalDate.now());
        c.add(LocalDate.of(2000, 5, 12));
        c.add(LocalDate.of(1997, 9, 18));
        c.add(LocalDate.of(2000, 5, 11));
        //ArrayUtil.selectionSort(c); // Nie działa dla LocalDate, naprawić
        System.out.println(c);
    }
}
