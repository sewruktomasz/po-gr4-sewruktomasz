package pl.edu.uwm.wmii.sewruktomasz.labolatorium11;


public class PairUtilDemo {
    public static void main(String[] args) {
        Pair<String> p = new Pair<>("Tomasz", "Sewruk");
        Pair<String> new_p = PairUtil.swap(p);
        System.out.println(String.format("%s %s", new_p.getFirst(), new_p.getSecond()));
    }
}
