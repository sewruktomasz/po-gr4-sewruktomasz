package pl.edu.uwm.wmii.sewruktomasz.labolatorium11;

public class PairUtil {
    public static <T> Pair<T> swap(Pair<T> p) {
        return new Pair(p.getSecond(), p.getFirst());
    }
}
