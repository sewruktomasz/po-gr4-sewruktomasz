package pl.edu.uwm.wmii.sewruktomasz.labolatorium11;


import java.util.*;

public class ArrayUtil<T> {
    public static <T extends Comparable<T>> boolean isSorted(ArrayList<T> a) {
        ArrayList<T> b = (ArrayList<T>) a.clone();
        b.sort(new Comparator<T>() {
            public int compare(T o1, T o2) {
                return o1.compareTo(o2);
            }
        });
        return a.equals(b);
    }
    public static <T extends Comparable<T>> int binSearch(ArrayList<T> a, T searched) {
        int l = 0;
        int p = a.size() - 1;
        int pivot = p / 2;
        while (l != p) {
            if (a.get(pivot).compareTo(searched) == -1)
                l = pivot;
            else if (a.get(pivot).compareTo(searched) == 1)
                p = pivot;
            else return pivot;
            pivot = (p + l) / 2;
        }
        return a.get(l).equals(searched) ? l : -1;
    }
    public static <T extends Comparable<T>> void selectionSort(ArrayList<T> a) {
        T min;
        for (int i = 0; i < a.size(); i++) {
            min = Collections.min(a.subList(i, a.size()));
            for (int j = i + 1; j < a.size(); j++) {
                if (a.get(j).equals(min)) {
                    Collections.swap(a, i, j);
                }
            }
        }
    }

}
