package pl.edu.uwm.wmii.sewruktomasz.labolatorium04;

public class RachunekBankowy {
    public RachunekBankowy(double wprowadzSaldo){
        saldo = wprowadzSaldo;
    }

    public static double rocznaStopaProcentowa = 0.0;
    private double saldo = 0.0;

    public double obliczMiesieczneOdsetki(){
        return (saldo * rocznaStopaProcentowa) / 12;
    }

    public void dodajDoSalda(){
        saldo += obliczMiesieczneOdsetki();
    }

    public static void setRocznaStopaProcentowa(double nowaWartosc){
        rocznaStopaProcentowa = nowaWartosc;
    }

    public static void main(String[] args){
            RachunekBankowy server1 = new RachunekBankowy(2000);
            RachunekBankowy server2 = new RachunekBankowy(3000);
            RachunekBankowy server3 = new RachunekBankowy(10000.0);
            RachunekBankowy.rocznaStopaProcentowa = 0.04;

            System.out.println("server1 odsetki = "+server1.obliczMiesieczneOdsetki());
            System.out.println("server2 odsetki = "+server2.obliczMiesieczneOdsetki());

            server1.dodajDoSalda();
            server2.dodajDoSalda();
            System.out.println("\nserver1 saldo = "+server1.saldo);
            System.out.println("server2 saldo = "+server2.saldo);
//-----------------------------------------------------------------
            RachunekBankowy.rocznaStopaProcentowa = 0.05;

            System.out.println("\nserver1 odsetki = "+server1.obliczMiesieczneOdsetki());
            System.out.println("server2 odsetki = "+server2.obliczMiesieczneOdsetki());

            server1.dodajDoSalda();
            server2.dodajDoSalda();
            System.out.println("\nserver1 saldo = "+server1.saldo);
            System.out.println("server2 saldo = "+server2.saldo);
    }
}
