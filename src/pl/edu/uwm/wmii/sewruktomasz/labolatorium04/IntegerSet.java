package pl.edu.uwm.wmii.sewruktomasz.labolatorium04;


public class IntegerSet {
    public IntegerSet() {
        this.set = new boolean[101];
        for (int i = 0; i < set.length; i++)
            this.set[i] = false;
    }
    public IntegerSet(Integer[] intSet) {
        for (int i = 0; i < set.length; i++)
            this.set[i] = false;
        for (int l : intSet)
            this.set[l] = true;
    }
    public IntegerSet(boolean[] boolSet) {
        for (int i = 0; i <= 100; i++)
            this.set[i] = boolSet[i];
    }
    public static IntegerSet union(IntegerSet set1, IntegerSet set2) {
        boolean[] result = new boolean[101];
        result[0] = false;
        for (int i = 1; i <= 100; i++)
            result[i] = set1.set[i] || set2.set[i];
        return new IntegerSet(result);
    }
    public static IntegerSet intersection(IntegerSet set1, IntegerSet set2) {
        boolean[] result = new boolean[101];
        result[0] = false;
        for (int i = 1; i <= 100; i++)
            result[i] = set1.set[i] && set2.set[i];
        return new IntegerSet(result);
    }
    public void insertElement(int el) {
        this.set[el] = true;
    }
    public void deleteElement(int el) {
        this.set[el] = false;
    }
    @Override
    public String toString() {
        StringBuffer result = new StringBuffer("");
        for (int i = 1; i <= 100; i++)
            if (this.set[i]) {
                result.append(i);
                result.append(" ");
            }
        return result.toString();
    }
    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject)
            return true;
        if (otherObject == null)
            return false;
        if (!(otherObject instanceof IntegerSet))
            return false;
        IntegerSet other = (IntegerSet) otherObject;

        for (int i = 1; i <= 100; i++)
            if (this.set[i] != other.set[i])
                return false;
        return true;
    }

    public boolean[] set;
}