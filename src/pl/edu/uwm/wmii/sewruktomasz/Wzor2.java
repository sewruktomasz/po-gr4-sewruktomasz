package pl.edu.uwm.wmii.sewruktomasz;
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class Wzor2 {
    public static void main(String[] arg){
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj wielkosc tablicy: ");
        int n = in.nextInt();
        int[] tab = generowanieTablicy(n);

        System.out.println("Suma dodatnich = "+dodatnie(tab)+"\nSuma ujemnych = "+ujemne(tab));

        System.out.println("top: "+najwieksza(tab)+" bottom: "+najmniejsza(tab));
    }

    public static int[] generowanieTablicy(int n){
        int[] tab = new int[n];
        Random rand = new Random();
        for(int i = 0; i<tab.length;i++){
            tab[i] = rand.nextInt(1331+1)-777; //rand.nextInt((max - min) + 1) + min;
        }
        return tab;
    };


    public static int dodatnie(int[] tab){
        int suma = 0;
        for(int i:tab)
            if(i>0) suma+=i;
        return suma;
    }

    public static int ujemne(int[] tab){
        int suma = 0;
        for(int i:tab)
            if(i<0) suma+=i;
        return suma;
    }



    public static int najwieksza(int[] tab){
        int najwieksza = tab[0];
        for (int i:tab)
            if(i>najwieksza) najwieksza = i;
        return najwieksza;
    }
    public static int najmniejsza(int[] tab){
        int najmniejsza = tab[0];
        for (int i:tab)
            if(i<najmniejsza) najmniejsza = i;
        return najmniejsza;
    }

}
