package pl.edu.uwm.wmii.sewruktomasz.laboratorium01;

import java.util.Scanner;

public class Zadanie2c {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt(),licznik=0;
        double liczba;

        for(int i=0;i<x;i++)
        {
            liczba=in.nextDouble();
            if(Math.sqrt(liczba)%2==0)
                licznik++;
        }
        System.out.print(licznik);
    }
}
