package pl.edu.uwm.wmii.sewruktomasz.laboratorium01;

import java.util.Scanner;

public class Wzor3 {
    public static void main(String[] arg) {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilosc liczb: ");
        int n = in.nextInt();

        int liczba;
        int licznik = 0;

        for (int i = 0; i < n; i++)
        {
            liczba = in.nextInt();
            if (liczba%2 == 1 && liczba%3 == 0)
                licznik++;


            System.out.println("Liczb nieparzystych podzielnych przez 3: "+licznik);

        }
    }
}