package pl.edu.uwm.wmii.sewruktomasz.laboratorium01;

import java.util.Scanner;

public class Zadanie2e {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt(),licznik=0, silnia = 1;
        double liczba;

        for(int i=1;i<=x;i++)
        {
            silnia*=i;
            liczba=in.nextDouble();
            if(Math.pow(2,i)<liczba && liczba<silnia)
                licznik++;
        }
        System.out.print(licznik);
    }
}
