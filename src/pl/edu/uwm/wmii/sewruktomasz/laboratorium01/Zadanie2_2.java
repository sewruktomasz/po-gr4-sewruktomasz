package pl.edu.uwm.wmii.sewruktomasz.laboratorium01;

import java.util.Scanner;

public class Zadanie2_2 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt();
        double liczba, wyraz_poprzedni, wyraz_nastepny, wynik;

        for(int i=0;i<x;i++)
        {
            liczba=in.nextDouble();
            if(liczba>0) {
                wynik = 2 * liczba;
                System.out.println(wynik);
            }
        }

    }
}
