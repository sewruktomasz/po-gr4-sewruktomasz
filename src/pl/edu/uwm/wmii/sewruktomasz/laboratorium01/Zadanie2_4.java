package pl.edu.uwm.wmii.sewruktomasz.laboratorium01;

import java.util.Scanner;

public class Zadanie2_4 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt(), zero=0, dodatnie=0, ujemne=0;
        double liczba;


        for(int i=0;i<x;i++)
        {
            liczba=in.nextDouble();
            if(liczba==0)
                zero++;
            if(liczba>0)
                dodatnie++;
            else
                ujemne++;
        }
        System.out.print(dodatnie+" "+zero+" "+ujemne);

    }
}
