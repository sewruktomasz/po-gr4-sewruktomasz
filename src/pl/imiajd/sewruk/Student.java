package pl.imiajd.sewruk;

import pl.edu.uwm.wmii.sewruktomasz.labolatorium04_wyklad9.Osoba;

import java.time.LocalDate;

public class Student extends Osoba {
    String kierunek;

    public Student(String kierunek, String nazwisko, LocalDate rokUrodzenia) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public String getKierunek() {
        return kierunek;
    }

    @Override
    public String toString() {
        return super.toString() + " " + this.kierunek;
    }

}
