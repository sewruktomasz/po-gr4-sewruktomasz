package pl.imiajd.sewruk;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Flet extends Instrument {
    public Flet() { super("Qwas", LocalDate.of(2015, 12, 21)); }
    public Flet(String producent, LocalDate rokProdukcji) { super(producent, rokProdukcji); }
    public String dzwiek() {
        return "<dźwięk fletu>";
    }
}
