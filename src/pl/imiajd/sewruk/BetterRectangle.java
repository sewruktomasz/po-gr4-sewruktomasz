package pl.imiajd.sewruk;

import java.awt.Rectangle;

public class BetterRectangle extends Rectangle {
    public BetterRectangle(int x, int y, int height, int width) {
        super(x, y, width, height);
    }
    public double getPerimeter() {
        return 2 * (this.getWidth() + this.getHeight());
    }
    public double getArea() {
        return this.getWidth() * this.getHeight();
    }
}
