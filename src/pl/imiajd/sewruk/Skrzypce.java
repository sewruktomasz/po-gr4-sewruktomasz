package pl.imiajd.sewruk;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Skrzypce extends Instrument {
    public Skrzypce() { super("Yahama", LocalDate.of(1999,1,12)); }
    public Skrzypce(String producent, LocalDate rokProdukcji) { super(producent, rokProdukcji); }
    public String dzwiek() {
        return "<dźwięk skrzypiec>";
    }
}
