package pl.imiajd.sewruk;


public class Adres {
    String ulica, miasto, kod_pocztowy;
    int numer_domu, numer_mieszkania;

    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public void pokaz() {
        System.out.println("Adres: " + this.kod_pocztowy + " " + this.miasto);
        System.out.println("Ulica: " + this.ulica + " " + this.numer_domu + "/" + this.numer_mieszkania);
    }

    public boolean przed(Adres adresik) {
        return Integer.parseInt(this.kod_pocztowy) < Integer.parseInt(adresik.kod_pocztowy);
    }


    public static void main(String[] arg) {
        Adres _Adres = new Adres("Wiejska", 19, 3, "Warszawa", "10-101");

        _Adres.pokaz();
    }
}
