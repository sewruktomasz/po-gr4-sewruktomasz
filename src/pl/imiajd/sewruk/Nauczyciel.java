package pl.imiajd.sewruk;

import pl.edu.uwm.wmii.sewruktomasz.labolatorium04_wyklad9.Osoba;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Nauczyciel extends Osoba {
    double pensja;
    public Nauczyciel(double pensja, String nazwisko, LocalDate rokUrodzenia){
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public double getPensja() {
        return pensja;
    }

    @Override
    public String toString() {
        return super.toString()+" "+this.pensja;
    }
}
