package pl.imiajd.sewruk;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Fortepian extends Instrument {
    public Fortepian() { super("Stradiwariush", LocalDate.of(2000, 06, 6)); }
    public Fortepian(String producent, LocalDate rokProdukcji) { super(producent, rokProdukcji); }
    public String dzwiek() {
        return "<dźwięk fortepianu>";
    }
}
