package pl.imiajd.sewruk;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Osoba {
    String nazwisko;
    LocalDate rokUrodzenia;

    public Osoba(String nazwisko, LocalDate rokUrodzenia) {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    @Override
    public String toString() {
        return this.rokUrodzenia + " " + this.nazwisko;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getRokUrodzenia() {
        return rokUrodzenia+"";
    }


}
